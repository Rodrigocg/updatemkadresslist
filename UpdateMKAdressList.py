#!/bin/python3
import requests
import json
import ipaddress
from librouteros import connect, exceptions
import sys


def is_valid_ip(ip):
    """Verifica se o IP fornecido é válido (IPv4 ou IPv6)."""
    try:
        ipaddress.ip_address(ip)
        return True
    except ValueError:
        return False


def get_online_blacklist(blacklist_data):
    all_ips = set()
    duplicate_count = 0

    for item in blacklist_data:
        response = requests.get(item['url'])
        for line in response.text.split(item.get('delimiter', '\n')):
            line = line.strip()
            if is_valid_ip(line):
                ip_with_cidr = line + item.get('cidr', '')
                if ip_with_cidr in all_ips:
                    duplicate_count += 1
                all_ips.add(ip_with_cidr)

    if duplicate_count:
        print(f"Encontrados {duplicate_count} IPs duplicados nas listas.")
    return all_ips


def update_router(api, online_blacklist, chain_name, description):
    mikrotik_blacklist = set()

    try:
        rules = api.path('ip', 'firewall', 'address-list').select(key=('list', 'address'))
        for rule in rules:
            if rule.get('list') == chain_name:
                mikrotik_blacklist.add(rule.get('address'))
    except exceptions.TrapError:
        print("Erro ao obter a blacklist do MikroTik")

    ips_to_add = online_blacklist - mikrotik_blacklist
    ips_to_remove = mikrotik_blacklist - online_blacklist

    for ip in ips_to_add:
        api.path('ip', 'firewall', 'address-list').add(list=chain_name, address=ip, comment=description)

    for ip in ips_to_remove:
        rule_id = [rule['.id'] for rule in api.path('ip', 'firewall', 'address-list').select(key=('.id', 'address')) if
                   rule['address'] == ip][0]
        api.path('ip', 'firewall', 'address-list').remove(rule_id)


def main(connection_config_path, blacklist_config_path):
    with open(blacklist_config_path) as f:
        blacklist_config = json.load(f)

    online_blacklist = get_online_blacklist(blacklist_config['urls'])

    with open(connection_config_path) as f:
        routers = json.load(f)

    for router in routers:
        api = connect(
            username=router['user'],
            password=router['pass'],
            host=router['host'],
            port=router.get('port', 8728)  # Se a porta não estiver especificada, usamos 8728 como padrão.
        )
        description = blacklist_config.get('description', router['blacklist_chain_name'])
        update_router(api, online_blacklist, router['blacklist_chain_name'], description)
        api.close()


if __name__ == '__main__':
    if len(sys.argv) != 3:
        print("Uso: script.py <connection_config.json> <blacklist_config.json>")
    else:
        main(sys.argv[1], sys.argv[2])
